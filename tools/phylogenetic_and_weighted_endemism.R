setwd("C:/Users/AlexSkeels/Dropbox/landscape_ecology/Projects/Gen3sis/Liolaemus")
library(phylobase)
library(ape)
library(phytools)
library(raster)
library(bioregion)

# set this to the director with the scripts
library(phylobase)

calc_PE <- function(tree, sites_x_tips,presence=c("presence","abundance","probability")) {
  
  # add code to check that the values are correct for the presence type:
  # 0 or 1 for presence - this calculates PE (Rosauer et al 2009)
  # from 0 to 1 for probability - this calculates model weighted PE (Rosauer, in prep)
  # any value for abundance - this calculation is equivalent to BED (Cadotte & Davies 2010)
  
  #default value for presence
  if (is.na(presence)) {presence="presence"}
  
  # change to a phylobase phylo4 object
  if (class(tree) == "phylo") {tree <- phylo4(tree)}
  
  sites_x_branches <- data.frame(cbind(rep(0,nrow(sites_x_tips))))
  
  for (i in 1:nTips(tree)) {
    sites_x_branches[,i] <- sites_x_tips[,which(labels(tree)[i]==names(sites_x_tips))]
    names( sites_x_branches)[i] <- labels(tree)[i]
  }
  
  rm(sites_x_tips); gc()
  branch.labels <- as.character(labels(tree))
  branch.count <- length(labels(tree))
  
  # add names and occupancy columns for internal branches
  for (i in (nTips(tree)+1):branch.count) {
    branch.labels[i] <- paste("b",i,sep="")
    desc <- as.integer(descendants(tree,i, type="tips"))
    if (presence=="abundance") {
      branch_col <- as.numeric(apply(sites_x_branches[,desc],MARGIN=1,FUN=sum))
    } else if (presence=="presence") {
      branch_col <- as.numeric(apply(sites_x_branches[,desc],MARGIN=1,FUN=max))
    } else if (presence=="probability") {
      branch_col <- as.numeric(apply(sites_x_branches[,desc],MARGIN=1,FUN=parent.prob))
    }
    sites_x_branches[,i] <- branch_col
    names(sites_x_branches[i]) <- branch.labels[i]
    #cat(i,branch.labels[i],length(desc),"\n")
    gc(verbose=F)
  }
  
  #scale columns (branches) to sum to 1
  sites_x_branches <- apply(sites_x_branches,MARGIN=2,FUN=scale.to,1)
  
  #now scale branches to sum to their length
  branch.lengths <- as.numeric(edgeLength(tree,1:branch.count))
  branch.lengths[is.na(branch.lengths)] <- 0
  for (i in 1:length(branch.lengths)) {
    sites_x_branches[,i] <- sites_x_branches[,i] * branch.lengths[i]
  }
  
  PE.vec <- apply(sites_x_branches,MARGIN=1,FUN=sum,na.rm=T)
  PE <- data.frame(cbind(1:nrow(sites_x_branches),PE.vec))
  names(PE) <- c("site","PE")
  return(PE)
}


parent.prob <- function(probabilities) {
  # probabilities is a vector of values between 0 and 1
  # add code to check values are of correct type!
  parent.prob <- 1 - prod(1-probabilities)
  return(parent.prob)
}

scale.to <- function(vec,vec.sum) {
  #mat is a vector
  #this function rescales each the vector values to sum to 'vec.sum'
  vec.tot <- sum(vec,na.rm=TRUE)
  if (vec.tot > 0) {
    vec.out <- vec.sum*vec/vec.tot
  } else {
    vec.out <- rep(0,times=length(vec))  #should columns for tips / branches with no occurrences be removed?
  }
  return(vec.out)
}


weighted_endemism <- function(x){
  #species_richness <- rowSums(x)
  proportions_matrix <- apply(x, 2, FUN=function(y){y <- y/sum(y)})
  WE <- rowSums(proportions_matrix)
  WE_df <- data.frame(site =1:length(WE), WE=WE)
  return(WE_df)
}


# load the data

sim_dir <- "//ites-fedata.ethz.ch/Simulation/dlipsky/output_directed/Andes03/m5_lowrange2cluster2"

setwd(sim_dir)
files <- list.files()
files <- files[grepl("Andes03", files)]

for(i in 1:20){
  
  print(i)
  
  setwd(sim_dir)
  setwd(files[i])
  
  species_xy <- try(read.table("pa_matrices/species_pa_1.txt", check.names = F, header=T))
  if(class(species_xy)=="try-error"){next}
  tree_og <- read.nexus("phy.nex")
  tree_ex <- drop.fossil(tree_og)
  
  colnames(species_xy)[3:ncol(species_xy)] <- paste0("species", colnames(species_xy)[3:ncol(species_xy)])
  
  all(tree_og$tip.label %in% colnames(species_xy))
  all(tree_ex$tip.label %in% colnames(species_xy))
  
  tree_og$tip.label[which(!tree_og$tip.label %in% colnames(species_xy))]
  tree_ex$tip.label[which(!tree_ex$tip.label %in% colnames(species_xy))]
  
  tree <- drop.tip(tree_ex, tree_og$tip.label[which(!tree_og$tip.label %in% colnames(species_xy))])
  
  pa_mat <- cbind(species_xy[,1:2],species_xy[,which(colnames(species_xy) %in% tree$tip.label)])
  
  tree <- phylo4(tree)
  
  phylo_PE <- calc_PE(tree,pa_mat,"presence") # call calc_PE, omiting the site name column from the matrix
  weighted_E <- weighted_endemism(pa_mat[, 3:ncol(pa_mat)])
  
  endemism <- cbind( species_xy[,1:2], phylo_PE, weighted_E[,2])
  
  PE <- rasterFromXYZ(endemism[, c(1:2, 4)])
  WE <- rasterFromXYZ(endemism[, c(1:2, 5)])
  
  plot(PE) 
  plot(WE) 
  
  saveRDS(endemism, file.path(sim_dir, "_Phylogenetic_Endemism", paste0(files[i], "_PE.rds")))
  
}


